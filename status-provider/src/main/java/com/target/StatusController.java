package com.target;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {
	
	@RequestMapping(value="/status", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public Status getStatus(){
		return new Status("OK", 20);
	}
}
