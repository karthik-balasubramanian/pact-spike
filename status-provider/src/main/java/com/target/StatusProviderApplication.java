package com.target;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatusProviderApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(StatusProviderApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(StatusProviderApplication.class, args);
		logger.info("--------- Provider App is running ---------");
	}
}
