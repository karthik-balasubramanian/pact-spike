package com.target;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import au.com.dius.pact.provider.junit.PactRunner;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;

@RunWith(PactRunner.class)
@Provider("status_provider")
@PactFolder("../pacts")
public class StatusProviderApplicationTests {
	
	@TestTarget
	public final Target target = new HttpTarget("http", "localhost", 8080);
	
	@SuppressWarnings("unused")
	private static ConfigurableWebApplicationContext application;
	
	@BeforeClass
	public static void start() {
	    application = (ConfigurableWebApplicationContext) 
	      SpringApplication.run(StatusProviderApplication.class);
	}
	
	@State("default")
	public void toGetState() { }
	
	
}
