package com.target;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Rule;
import org.junit.Test;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

public class StatusConsumerApplicationTests {

	@Rule
	public PactProviderRuleMk2 pactProviderRule = new PactProviderRuleMk2("status_provider", this);

	@Pact(consumer = "status-consumer-demo")
	public RequestResponsePact createFragment(PactDslWithProvider builder) {

		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json;charset=UTF-8");
		
		PactDslJsonBody jsonBody = new PactDslJsonBody()
			    .stringType("status", "OK")
			    .numberValue("exceptionCount", 20);

		//body("{\"status\": \"OK\", \"exceptionCount\": \"20\"}")
		return builder.given("default").uponReceiving("Test interaction").path("/status").method("GET")
				.willRespondWith().status(200).headers(headers).body(jsonBody)
				.toPact();
	}

	@Test
	@PactVerification(value = "status_provider", fragment = "createFragment")
	public void runTest() throws Exception {
		String url = pactProviderRule.getUrl();
		URIBuilder uriBuilder = new URIBuilder(url);
		URI statusUri = uriBuilder.setPath(uriBuilder.getPath() + "status").build().normalize();
		StatusClient client = new StatusClient();
		Status result = client.getStatus(statusUri);

		assertEquals("OK", result.getStatus());
		assertEquals(20, result.getExceptionCount());
	}
}
