package com.target;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class StatusConsumerApplication implements CommandLineRunner{
	
	private static final Logger logger = LoggerFactory.getLogger(StatusConsumerApplication.class);
	  
	@Autowired
	private StatusClient client;
	
	@Override
	public void run(String... args) throws Exception {
		URI providerURI = URI.create("http://localhost:8080/status");
		logger.info("--------- Making REST call to provider ---------");
		Status status = client.getStatus(providerURI);
		logger.info("--------- Received response from provider ---------");
		logger.info("--------- Response : " + status.toString() + "--------- ");
	}
	
	public static void main(String[] args) throws Exception {
        SpringApplication.run(StatusConsumerApplication.class, args);
    }

}
