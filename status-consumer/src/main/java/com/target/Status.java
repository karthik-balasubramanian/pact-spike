package com.target;

public class Status {
	
	private String status;
	private int exceptionCount;
	
	public Status(String status, int exceptionCount) {
		super();
		this.status = status;
		this.exceptionCount = exceptionCount;
	}

	public Status(){}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public int getExceptionCount() {
		return exceptionCount;
	}

	public void setExceptionCount(int exceptionCount) {
		this.exceptionCount = exceptionCount;
	}

}
