package com.target;

import java.net.URI;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StatusClient {
	
	
	public Status getStatus(URI providerURI) {
		return new RestTemplate().getForObject(providerURI, Status.class);
	}
}
